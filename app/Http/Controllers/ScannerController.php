<?php

namespace App\Http\Controllers;

use App\Library\Rule;
use Illuminate\Http\Request;

class ScannerController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function form(){
        return view('form');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function results(Request $request){

        $request->validate([
            'file' => 'required|mimes:csv,txt'
        ]);

        $tmpFilePath = $request->file->getRealPath();

        $fileContent = array_map('str_getcsv', file($tmpFilePath));

        $results = collect($fileContent)->map(function($row){
            $row[] = (new Rule($row))->isClaimable() ? 'Y' : 'N';
            return $row;
        });

        return view('results', compact('results'));
    }
}
