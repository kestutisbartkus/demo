<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Upload</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link href="/css/app.css" type="text/css" rel="stylesheet">

</head>
<body>

    <div class="container">
        <div class="row py-5">
            <div class="col col-lg-6 offset-lg-3">
                <form action="{{route('scanner.results')}}" method="post" enctype="multipart/form-data">
                    @csrf

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="form-group">
                        <label for="file">CSV failas</label>
                        <input type="file" name="file" class="form-control" id="file" required>
                    </div>

                    <div class="form-group text-center">
                        <button class="btn btn-primary">Siųsti</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="/js/app.js"></script>
</body>
</html>
