<?php
/**
 * Created by PhpStorm.
 * User: napalias
 * Date: 26/07/2018
 * Time: 22:17
 */

namespace App\Library;


class Rule
{
    protected $europa = ['AT', 'BE', 'BG', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'GB'];

    /*
     * result[0] = country
     * result[1] = status
     * result[2] = status details
     */
    protected $result = [];


    /**
     * Rule constructor.
     * @param $result
     */
    public function __construct($result)
    {
        $this->result = array_map(function ($item) {
            return trim($item);
        }, $result);
    }

    public function isClaimable()
    {
        if (!$this->isEuropa()) {
            return false;
        }

        if ($this->isCanceled14()) {
            return true;
        }

        if ($this->isDelay3()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function isEuropa()
    {
        return in_array($this->result[0], $this->europa);
    }

    /**
     * @return bool
     */
    protected function isCanceled14()
    {
        return $this->result[1] == 'Cancel' && (int)$this->result[2] <= 14;
    }

    /**
     * @return bool
     */
    protected function isDelay3()
    {
        return $this->result[1] == 'Delay' && (int)$this->result[2] >= 3;
    }
}