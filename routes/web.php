<?php
Route::get('/', 'ScannerController@form');
Route::post('results', 'ScannerController@results')->name('scanner.results');