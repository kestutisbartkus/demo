<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Upload</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <link href="/css/app.css" type="text/css" rel="stylesheet">

</head>
<body>

<div class="container">
    <div class="row py-5">
        <div class="col col-lg-6 offset-lg-3">
            <table class="table">
                @foreach($results as $result)
                    <tr>
                        <td>{{$result[0]}}</td>
                        <td>{{$result[1]}}</td>
                        <td>{{$result[2]}}</td>
                        <td>{{$result[3]}}</td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
<script src="/js/app.js"></script>
</body>
</html>
